import java.util.Objects;

public class Tuple<K, V> {
    public final K First;
    public final V Second;

    public Tuple(K k, V v) {

        this.First = k;
        this.Second = v;
    }

    @Override
    public int hashCode() {
        return First.hashCode() +
                Second.hashCode() * 397;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Tuple) {
            Tuple<K, V> other = (Tuple<K, V>) obj;
            return Objects.equals(First, other.First) && Objects.equals(Second, other.Second);
        } else
            return false;
    }
}
