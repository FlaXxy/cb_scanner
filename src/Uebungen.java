import java.util.ArrayList;

public class Uebungen {
    public static void main(String[] args){
        var uebungen=new Uebungen();
        uebungen.Uebung1d();
        uebungen.Uebung2c();
    }
    public void Uebung1d(){
        var scanner = Scanner.create()
                .start("Start")
                .end("Vorkomma")
                .end("Nachkomma")
                .transition("Start",'0',"Vorkomma")
                .transition("Start",'1',"Vorkomma")
                .transition("Start",'2',"Vorkomma")
                .transition("Start",'3',"Vorkomma")
                .transition("Start",'4',"Vorkomma")
                .transition("Start",'5',"Vorkomma")
                .transition("Start",'6',"Vorkomma")
                .transition("Start",'7',"Vorkomma")
                .transition("Start",'8',"Vorkomma")
                .transition("Start",'9',"Vorkomma")
                .transition("Start",'.',"Punkt")
                .transition("Vorkomma",'0',"Vorkomma")
                .transition("Vorkomma",'1',"Vorkomma")
                .transition("Vorkomma",'2',"Vorkomma")
                .transition("Vorkomma",'3',"Vorkomma")
                .transition("Vorkomma",'4',"Vorkomma")
                .transition("Vorkomma",'5',"Vorkomma")
                .transition("Vorkomma",'6',"Vorkomma")
                .transition("Vorkomma",'7',"Vorkomma")
                .transition("Vorkomma",'8',"Vorkomma")
                .transition("Vorkomma",'9',"Vorkomma")
                .transition("Vorkomma",'.',"Nachkomma")
                .transition("Nachkomma",'0',"Nachkomma")
                .transition("Nachkomma",'1',"Nachkomma")
                .transition("Nachkomma",'2',"Nachkomma")
                .transition("Nachkomma",'3',"Nachkomma")
                .transition("Nachkomma",'4',"Nachkomma")
                .transition("Nachkomma",'5',"Nachkomma")
                .transition("Nachkomma",'6',"Nachkomma")
                .transition("Nachkomma",'7',"Nachkomma")
                .transition("Nachkomma",'8',"Nachkomma")
                .transition("Nachkomma",'9',"Nachkomma")
                .transition("Punkt",'0',"Nachkomma")
                .transition("Punkt",'1',"Nachkomma")
                .transition("Punkt",'2',"Nachkomma")
                .transition("Punkt",'3',"Nachkomma")
                .transition("Punkt",'4',"Nachkomma")
                .transition("Punkt",'5',"Nachkomma")
                .transition("Punkt",'6',"Nachkomma")
                .transition("Punkt",'7',"Nachkomma")
                .transition("Punkt",'8',"Nachkomma")
                .transition("Punkt",'9',"Nachkomma")
                .build();
        if (scanner.scan(".223")) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }

    public void Uebung2c(){
        Character[] ziffer={'0',
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9',
        };

        var scanner = Scanner.create()
                .start("z1")
                .end("z2")
                .end("z4")
                .end("z7")
                .transition("z1",ziffer,"z2")
                .transition("z2",ziffer,"z2")
                .transition("z2",'.',"z2")
                .transition("z2",'E',"z5")
                .transition("z3",ziffer,"z4")
                .transition("z4",ziffer,"z4")
                .transition("z4",'E',"z5")
                .transition("z5",'+',"z6")
                .transition("z5",'-',"z6")
                .transition("z5",ziffer,"z7")
                .transition("z6",ziffer,"z7")
                .transition("z7",ziffer,"z7")
                .build();
        if (scanner.scan("1223.0E23")) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
