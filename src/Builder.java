import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Builder {
    private final HashMap<Entry, String> _map = new HashMap<>();
    private final Set<String> _ends = new HashSet<>();
    private String _start;

    public Scanner build() {
        return new Scanner(_map, _start, _ends);
    }

    public Builder transition(String state, Character character, String newState) {
        _map.put(new Entry(state, character), newState);
        return this;
    }
    public Builder transition(String state, Character[] characters, String newState) {
        for (Character c : characters) {
            _map.put(new Entry(state, c), newState);
        }
        return this;
    }

    public Builder end(String state) {
        _ends.add(state);
        return this;
    }

    public Builder start(String start) {
        _start = start;
        return this;
    }
}