import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Scanner {
    public static Builder create() {
        return new Builder();
    }

    Scanner(Map<Entry, String> map, String start, Set<String> ends) {
        _map = map;
        _start = start;
        _ends = ends;
    }

    private final Map<Entry, String> _map;
    private String _start;
    private Set<String> _ends;

    public boolean scan(String word) {
        String state = _start;
        for (int i = 0; i < word.length(); ++i) {
            char character = word.charAt(i);

            state = _map.getOrDefault(new Entry(state, character), null);
            if (state == null)//trap; abort now
                return false;
        }
        return _ends.contains(state);
    }

}
